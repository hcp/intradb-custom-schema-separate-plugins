package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbGurSchemaPlugin",
			name = "Intradb Gur Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbGurSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbGurSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Gur Schema plugin.
	 */
	public IntradbGurSchemaPlugin() {
		logger.info("Configuring Intradb Gur Schema plugin");
	}
	
}
