package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbToolboxSchemaPlugin",
			name = "Intradb Toolbox Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbToolboxSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbToolboxSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Toolbox Schema plugin.
	 */
	public IntradbToolboxSchemaPlugin() {
		logger.info("Configuring Intradb Toolbox Schema plugin");
	}
	
}
