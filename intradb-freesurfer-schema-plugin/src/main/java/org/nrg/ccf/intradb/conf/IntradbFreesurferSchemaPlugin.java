package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbFreesurferSchemaPlugin",
			name = "Intradb Freesurfer Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbFreesurferSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbFreesurferSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Freesurfer Schema plugin.
	 */
	public IntradbFreesurferSchemaPlugin() {
		logger.info("Configuring Intradb Freesurfer Schema plugin");
	}
	
}
