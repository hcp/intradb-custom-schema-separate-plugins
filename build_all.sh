#!/bin/bash

#find . -mindepth 1 -maxdepth 1 -type d | egrep -v "new-|\.git" | egrep -v "radread"| xargs -I '{}' /bin/bash -c "pushd {};/nrgpackages/tools/gradle/bin/gradle clean jar publishToMavenLocal;popd"
echo "IMPORTANT:  There is currently a bug in the gradle daemon.  It must be stopped before each jar is created.  Otherwise contents from different plugins are getting mixed together." 
sleep 3
find . -mindepth 1 -maxdepth 1 -type d | egrep -v "new-|\.git" | egrep -v "radread"| xargs -I '{}' /bin/bash -c "pushd {};gradle --stop;gradle clean jar publishToMavenLocal;popd"

