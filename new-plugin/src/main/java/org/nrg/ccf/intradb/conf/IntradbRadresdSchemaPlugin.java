package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbRadreadSchemaPlugin",
			name = "Intradb RadRead Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntadbRadreadSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntadbRadreadSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb RadRead Schema plugin.
	 */
	public IntadbRadreadSchemaPlugin() {
		logger.info("Configuring Intradb RadRead Schema plugin");
	}
	
}
