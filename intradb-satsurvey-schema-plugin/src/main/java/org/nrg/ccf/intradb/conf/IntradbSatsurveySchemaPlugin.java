package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbSatsurveySchemaPlugin",
			name = "Intradb SatSurvey Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbSatsurveySchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbSatsurveySchemaPlugin.class);

	/**
	 * Instantiates a new Intradb SatSurvey Schema plugin.
	 */
	public IntradbSatsurveySchemaPlugin() {
		logger.info("Configuring Intradb SatSurvey Schema plugin");
	}
	
}
