package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbRestrictedIdsSchemaPlugin",
			name = "Intradb Restricted Ids Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbRestrictedIdsSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbRestrictedIdsSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Restricted Ids Schema plugin.
	 */
	public IntradbRestrictedIdsSchemaPlugin() {
		logger.info("Configuring Intradb Restricted Ids Schema plugin");
	}
	
}
