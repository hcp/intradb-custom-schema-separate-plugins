package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbNontoolboxSchemaPlugin",
			name = "Intradb Nontoolbox Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbNontoolboxSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbNontoolboxSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Nontoolbox Schema plugin.
	 */
	public IntradbNontoolboxSchemaPlugin() {
		logger.info("Configuring Intradb Nontoolbox Schema plugin");
	}
	
}
