package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbVisitFormSchemaPlugin",
			name = "Intradb Visit Form Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbVisitFormSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbVisitFormSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Visit Form Schema plugin.
	 */
	public IntradbVisitFormSchemaPlugin() {
		logger.info("Configuring Intradb Visit Form Schema plugin");
	}
	
}
