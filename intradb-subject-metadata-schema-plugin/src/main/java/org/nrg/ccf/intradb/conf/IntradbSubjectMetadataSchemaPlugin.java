package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbSubjectMetadataSchemaPlugin",
			name = "Intradb Subject Metadata Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbSubjectMetadataSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbSubjectMetadataSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Subject Metadata Schema plugin.
	 */
	public IntradbSubjectMetadataSchemaPlugin() {
		logger.info("Configuring Intradb Subject Metadata Schema plugin");
	}
	
}
