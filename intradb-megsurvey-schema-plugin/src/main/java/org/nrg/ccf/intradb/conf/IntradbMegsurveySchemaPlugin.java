package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbMegsurveySchemaPlugin",
			name = "Intradb MegSurvey Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbMegsurveySchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbMegsurveySchemaPlugin.class);

	/**
	 * Instantiates a new Intradb MegSurvey Schema plugin.
	 */
	public IntradbMegsurveySchemaPlugin() {
		logger.info("Configuring Intradb MegSurvey Schema plugin");
	}
	
}
