package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbSsagaSchemaPlugin",
			name = "Intradb SSAGA Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbSsagaSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbSsagaSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb SSAGA Schema plugin.
	 */
	public IntradbSsagaSchemaPlugin() {
		logger.info("Configuring Intradb SSAGA Schema plugin");
	}
	
}
