package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbDelayDiscSchemaPlugin",
			name = "Intradb DelayDisc Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbDelayDiscSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbDelayDiscSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb DelayDisc Schema plugin.
	 */
	public IntradbDelayDiscSchemaPlugin() {
		logger.info("Configuring Intradb Delay Disc Schema plugin");
	}
	
}
