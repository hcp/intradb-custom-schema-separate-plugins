package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbSyntheticVariableSchemaPlugin",
			name = "Intradb Synthetic Variable Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbSyntheticVariableSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbSyntheticVariableSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb Synthetic Variable schema plugin.
	 */
	public IntradbSyntheticVariableSchemaPlugin() {
		logger.info("Configuring Intradb Synthetic Variable schema plugin");
	}
	
}
