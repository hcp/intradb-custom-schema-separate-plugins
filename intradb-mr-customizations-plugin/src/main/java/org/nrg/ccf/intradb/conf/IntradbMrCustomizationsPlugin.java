package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbMrCustomizationsPlugin",
			name = "Intradb MR Customizations Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbMrCustomizationsPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbMrCustomizationsPlugin.class);

	/**
	 * Instantiates a new Intradb MR Customizations plugin.
	 */
	public IntradbMrCustomizationsPlugin() {
		logger.info("Configuring Intradb MR Customizations plugin");
	}
	
}
