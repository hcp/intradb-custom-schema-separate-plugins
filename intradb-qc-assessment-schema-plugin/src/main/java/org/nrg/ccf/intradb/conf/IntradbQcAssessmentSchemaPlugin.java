package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbQcAssessmentSchemaPlugin",
			name = "Intradb QC Assessment Schema Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbQcAssessmentSchemaPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbQcAssessmentSchemaPlugin.class);

	/**
	 * Instantiates a new Intradb QC Assessment Schema plugin.
	 */
	public IntradbQcAssessmentSchemaPlugin() {
		logger.info("Configuring Intradb QC Assessment Schema plugin");
	}
	
}
